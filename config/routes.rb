Rails.application.routes.draw do
  resources :orders
  get 'welcome/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
  resources :products
  resources :orders, only: [:index,:show, :new ]
  post '/purchase' => 'orders#purchase_status'
  get '/refund/:id', to: 'orders#refund'
end
