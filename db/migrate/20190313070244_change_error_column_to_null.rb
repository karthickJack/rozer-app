class ChangeErrorColumnToNull < ActiveRecord::Migration[5.2]
  def change
  	change_column_null :orders, :error, true
  end
end
