class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :payment_id
      t.string :method
      t.integer :amount
      t.string :status
      t.text :error
      t.references :product, foreign_key: true
      t.references :user, foreign_key: true
      
      t.timestamps
    end
  end
end
