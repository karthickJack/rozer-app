class ChangeDatatype < ActiveRecord::Migration[5.2]
  def change
  	change_column :orders, :payment_id, :string
  end
end
