class OrdersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def purchase_status
    begin
      @order = Order.process_razorpayment(order_params)
      redirect_to :action => "show", :id => @order.id
    rescue Exception
      flash[:notice] = "Unable to process payment."
      redirect_to root_path
    end
  end

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find_by_id(params[:id])
  end

  def refund
    payment_id = Order.find_by_id(params[:id]).payment_id
    @order = Order.process_refund(payment_id)
    redirect_to :action => "show", :id => @order.id
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
       p = params.permit(:razorpay_payment_id, :payment_id, :method, :amount, :status, :error, :product_id, :user_id)
       p.merge!({payment_id: p.delete(:razorpay_payment_id) || p[:payment_id]})
       p
    end
end
