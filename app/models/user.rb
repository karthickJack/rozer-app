class User < ApplicationRecord
  has_many :orders
  include Clearance::User
end
