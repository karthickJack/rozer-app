json.extract! order, :id, :payment_id, :method, :amount, :status, :error, :product_id, :user_id, :created_at, :updated_at
json.url order_url(order, format: :json)
